#!/bin/bash
# only do this for interactive shells
if [ "$-" != "${-#*i}" ]; then
        if [ -f "$HOME/.not_logged_in_yet" ]; then
                xdg-user-dirs-update
        fi
        
        if [ ! -f "$HOME/.face.icon" ]; then
            cp /etc/skel/.face "$HOME/.face"
            ln -fs "$HOME/.face" "$HOME/.face.icon"
        fi
	
	# exec conditional check based on dmesg to see if running in vmware on first login
	# caveats - requires sudo, only checks first go, if switching platforms on existing account, will not update
	# the below 2 lines should not be appended to user's config not running vmware
	echo -ne "\nPerforming check on first login only to see if running VMWare - this requieres sudo - please enter your password below\n"
        
        if sudo dmesg | grep DMI | grep -iq vmware; then
                echo -ne "\nVMWare detected, attempting open-vm-tools installation and i3-config tweak for VMware.\n"
            sudo apt -qq -y install open-vm-tools open-vm-tools-desktop
            echo -ne "\n#try to fix copy/paste / VMWare Install Tools if using vmware - might not support drag & drop" >> "$HOME/.config/i3/config"
            echo -ne "\nexec vmware-user-suid-wrapper --no-startup-id\n" >> "$HOME/.config/i3/config"
        else
            echo -ne "\nVMWare not detected, skipping open-vm-tools installation and i3-config tweak for VMware.\n"
	fi
        rm "$HOME/.not_logged_in_yet"
fi
