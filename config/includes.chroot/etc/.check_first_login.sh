
# only do this for interactive shells
if [ "$-" != "${-#*i}" ]; then
    if [ -f "$HOME/.not_logged_in_yet" ]; then
        xdg-user-dirs-update
        if [ ! -f "$HOME/.face.icon" ]; then
            cp /etc/skel/.face "$HOME/.face"
            ln -fs "$HOME/.face" "$HOME/.face.icon"
        fi
        rm "$HOME/.not_logged_in_yet"
    fi
fi

